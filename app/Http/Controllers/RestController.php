<?php

namespace App\Http\Controllers;

use App\Http\Requests\HumanUpdateRequest;
use App\Models\human;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class RestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $humans = human::all();
        return view('index' ,compact('humans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HumanUpdateRequest $request)
    {
        $file_name = Str::random(32) . "." . $request->photo->extension();
        Storage::disk('public')->putFileAs('photo',$request->photo,$file_name);

        Human::create([
            'photo'=>$file_name ,
            'name'=>$request->name ,
            'about'=>$request->about ,
        ]);
        return redirect('/Humans');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $humans = human::findOrFail($id);
        return view('show',compact('humans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $humans = human::findOrFail($id);

        return view('edit',compact('humans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HumanUpdateRequest $request, $id)
    {
       // $validatedData = $request->validate($rules);
       // dd($validatedData);
        $humans = human::findOrFail($id);
        if ($request->photo){

            $file_name = Str::random(32) . "." . $request->photo->extension();

            Storage::disk('public')->putFileAs('photo', $request->photo, $file_name);

        }
        $humans->photo = $file_name;
        $humans->name = $request->name;
        $humans->about = $request->about;

        $humans->save();

        return redirect("/Humans/");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $human = human::findOrFail($id);
        $human -> delete();
        return redirect('/Humans');
    }
    public function test(){
        $query_builder = human::where('id','>', 26)->orderBY('id','DESC');
        return $query_builder->get();
    }
}
