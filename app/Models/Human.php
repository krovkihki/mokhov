<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class human extends Model
{
    public $timestamps = false;
    public $fillable = [
        'photo' ,
        'name' ,
        'about' ,
];
    public function getphotoAttribute($value) {
        if (is_null($value)) {
            return null;
        }else{
            return url('storage/photo/' . $value);
        }
    }
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
public function scopeMoreThan($query)
    {
    return $query->select($this->getAdminFields());
    }
}
