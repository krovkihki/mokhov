<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;
    public $fillable = [
        'id' ,
        'human_id' ,
        'text' ,
        ];
}
