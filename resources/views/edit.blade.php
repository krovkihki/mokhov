
@section('content')
    <h1> Редактирование человека №{{$humans-> id}} </h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <h1><li><font color="red">{{ $error }}</li> </font></h1>
                @endforeach
            </ul>
        </div>
    @endif

    <a href="/Humans">
    К списку
</a>
   <form  action="/Humans/{{$humans->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <input type="file" name="photo" >
        <td><img src="{{$humans->photo }}"alt=""> </td>
        <input type="text" name="name" placeholder="ФИО" required value="{{$humans-> name}}">
        <input type="text" name="about" placeholder="Описание" required value="{{$humans-> about}}">
        <input type="submit" value="Сохранить">
    </form>



