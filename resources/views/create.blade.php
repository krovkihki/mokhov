
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <h1><li><font color="red">{{ $error }}</li> </font></h1>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>
        Создание сотрудника
    </h1>
    <a href ="/Humans">
        к списку
    </a>
    <form action="/Humans" method ="POST" enctype="multipart/form-data" >
        @csrf
        <input type="file" name="photo" required>
        <input type="text" name="name" placeholder="ФИО" required>
        <input type="text" name="about" placeholder="Описание" required>
        <input type="submit" value="Создать">

    </form>

